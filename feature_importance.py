import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import normalize

file = 'track_features.csv'

feature_df = pd.read_csv(file)

feature_df = feature_df.drop('Unnamed: 0', axis=1)

g = feature_df.groupby('genre').size().reset_index(name='counts').sort_values(by=[u'counts'],axis=0).tail(8)


for col in feature_df['genre'].unique():
	if col not in g['genre'].tolist():
		feature_df = feature_df.drop(feature_df.index[feature_df['genre']==col].tolist())

columns = feature_df.columns

X = feature_df.drop('genre', axis=1)
columns = X.columns

X = X.astype(float)
X = normalize(X)
Y = feature_df['genre']

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=23)

clf = RandomForestClassifier(n_estimators=100)

clf.fit(X_train, Y_train)

feature_importances = pd.DataFrame(clf.feature_importances_,
                                   index = columns,
                                    columns=['importance']).sort_values('importance',                                                                 ascending=False)

print feature_importances

