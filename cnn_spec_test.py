import os
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import MultiLabelBinarizer, LabelEncoder, StandardScaler
import sklearn as skl
import matplotlib.pyplot as plt
from PIL import Image
import cv2

def get_specs():

    tracks_file = os.path.join(".", "datatest", "tracks.csv")

    tracks_df = pd.read_csv(tracks_file, usecols = [u'Unnamed: 0', u'track.7'], dtype = {u'Unnamed: 0' : str, u'track.7' : str})
    tracks_df = tracks_df.rename(columns={u'Unnamed: 0': 'track_id', u'track.7': 'genre'})

    # put together features & genre information - skip all without genre
    final_df = tracks_df[['track_id', 'genre']]

    # exclude lines with all blanks
    final_df['genre'].replace('', np.nan, inplace=True)
    final_df['track_id'].replace('', np.nan, inplace=True)
    
    # exclude data without genres 
    final_df = final_df.dropna(subset=['genre'])
    final_df = final_df.dropna(subset=['track_id'])

    # set the index to track_id
    final_df.set_index("track_id", inplace=True)

    # convert indexes to str
    final_df.index = final_df.index.map(str)

    Y_train = []
    X_train = []

    
    dirname = os.path.join(".", "datatest", "spectrogram_images_small") #"./dataset/spectrogram_images_small"
    for root, dirs, files in os.walk(dirname):
        for f_ in files:
            try:
                filename = os.path.join(root, f_)
                Y_train.append(final_df.loc[str(int(f_[:-4])), "genre"])
                X_train.append(cv2.imread(filename, 0))
            except Exception as e:
                print(f_, e)
                pass

    return (np.asarray(X_train), np.asarray(Y_train))
if __name__ == "__main__":
    X, Y = get_specs()
    nclasses = len(set(Y))
    print nclasses
    print set(Y)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=23)

    print "x train shape"
    print X_train.shape
    print "image shape"
    print X_train[0].shape

    X_train = X_train.reshape(X_train.shape[0], X_train.shape[1], X_train.shape[2], 1)

    X_test = X_test.reshape(X_test.shape[0], X_test.shape[1], X_test.shape[2], 1) 
	
    imgsz = (X_train[0].shape[0], X_train[0].shape[1], 1)
   
    # normalize
    X_train = X_train / 255.0
    X_test = X_test / 255.0
 
    enc = LabelEncoder()
    Y_train = enc.fit_transform(Y_train)
    Y_test = enc.fit_transform(Y_test)

    model = tf.keras.models.load_model('spec_model.h5')

    test_loss, test_acc = model.evaluate(X_test, Y_test)
    print test_loss
    print test_acc 
 
