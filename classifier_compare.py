import sys
import pandas as pd
import IPython.display as ipd
import sklearn as skl
import sklearn.utils, sklearn.preprocessing, sklearn.decomposition, sklearn.svm
from sklearn.utils import shuffle
from sklearn.preprocessing import MultiLabelBinarizer, LabelEncoder, StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.multiclass import OneVsRestClassifier

features = pd.read_csv('features.csv', index_col=0, header=[0, 1, 2])
training = pd.read_csv('training_data.csv')
testing = pd.read_csv('testing_data.csv')

feats = ['mfcc', 'spectral_contrast', 'chroma_cens', 'spectral_centroid', 'zcr']

x_train = [features.loc[track_id, feats] for track_id in training['track_id']] 

y_train = training['track_genre_top']

x_test = [features.loc[track_id, feats] for track_id in testing['track_id']] 

y_test = testing['track_genre_top']

# encode labels as integers

enc = LabelEncoder()
y_train = enc.fit_transform(y_train)
y_test = enc.fit_transform(y_test)

x_train, y_train = skl.utils.shuffle(x_train, y_train, random_state=42)

classifiers = {
    'LR': LogisticRegression(),
    'kNN': KNeighborsClassifier(n_neighbors=200),
    'SVCrbf': SVC(kernel='rbf'),
    'SVCpoly1': SVC(kernel='poly', degree=1),
    'linSVC2': LinearSVC(),
    'DT': DecisionTreeClassifier(max_depth=5),
    'RF': RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
    'AdaBoost': AdaBoostClassifier(n_estimators=10),
    'MLP1': MLPClassifier(hidden_layer_sizes=(100,), max_iter=2000),
    'MLP2': MLPClassifier(hidden_layer_sizes=(200, 50), max_iter=2000),
    'NB': GaussianNB(),
    'QDA': QuadraticDiscriminantAnalysis(),
}

scores = dict()

scaler = skl.preprocessing.StandardScaler(copy=False)
scaler.fit_transform(x_train)
scaler.transform(x_test)

for clf_name, clf in classifiers.items():  
    print "Running: {}".format(clf_name)
    clf.fit(x_train, y_train)
    score = clf.score(x_test, y_test)
    scores[clf_name] = "score: {:.2%}".format(score)

print scores

