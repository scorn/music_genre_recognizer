import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.decomposition import PCA
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import MultiLabelBinarizer, LabelEncoder, StandardScaler
import sklearn as skl
from keras.applications import ResNet50
from PIL import Image
import matplotlib.pyplot as plt
import os

file_ = os.path.join(".", "datatest", "track_features.csv")
feature_df = pd.read_csv(file_)
X = feature_df.drop('genre', axis=1)
X = X.astype(float)
Y = feature_df['genre']


X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=23)

X_train = X_train.values
X_test = X_test.values
Y_train = Y_train.values
Y_test = Y_test.values

print X_train.shape

scaler = skl.preprocessing.StandardScaler(copy=False)
scaler.fit_transform(X_train)
scaler.transform(X_test)

expand_train= []
expand_test = []
imgsz = (26, 20)

for obj in X_train:
    expand_train.append(np.append(obj, np.array([0])).reshape(imgsz))

for obj in X_test:
    expand_test.append(np.append(obj, np.array([0])).reshape(imgsz))

X_train = np.array(expand_train)
X_test = np.array(expand_test)

X_train = X_train.reshape(X_train.shape[0], imgsz[0],imgsz[1], 1)
X_test = X_test.reshape(X_test.shape[0], imgsz[0], imgsz[1], 1) 

enc = LabelEncoder()
Y_train = enc.fit_transform(Y_train)
Y_test = enc.fit_transform(Y_test)

model = tf.keras.models.load_model('cnn_model.h5')

test_loss, test_acc = model.evaluate(X_test, Y_test)
print test_loss
print test_acc 


