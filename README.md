# Installation/Dependencies
You will need python 2.7.15 to run the classifers.

Here are all the python libraries you must have installed:
- pandas
- IPython.display as ipd
- sklearn 
- numpy
- tensorflow (version 1.1)
- matplotlib
- pillow
- opencv-python

# Data

You can download the fma dataset from https://github.com/mdeff/fma.
We have modified the csvs for our project so you should use the ones
included in this repo.

You can also find the spectogram_images_small directory at

https://drive.google.com/open?id=1o9y9fGDDRXStKOvVmYvvo4HJCK5r4AbN

# RFC

You can train and test the RFC model as follows:

cd datatest

python traintest.py

This will create a 'pickled' model that can be copied to /userinterface to be used:

cp rfc.p ../userinterface

# CNN (libROSA features)

You can train and create the model as follows:

python cnn.py

The model created is named cnn_model.h5

you can test the model without running the classifier as follows:

python cnn_test.py

# CNN (spectograms)

The cnn can be run as follows:

python cnn_spec.py

The model created is named spec_model.h5

you can test the model without running the classifier as follows:

python cnn_spec_test.py

# User Interface

The command line user interface can be run as follows:

python music_genre_recognizer.py [sample music file in .mp3 format]

