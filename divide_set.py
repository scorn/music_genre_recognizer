import sys
import random
import csv

def main():
    output_file_training_name = 'training_data.csv'
    output_file_testing_name = 'testing_data.csv'
    output_file_validation_name = 'validation_data.csv'
    
    if len(sys.argv)<2:
        print_help()
        exit(0)
    
    file_name = sys.argv[1]
    input_file = open(file_name,'rb')
    csv_reader = csv.DictReader(input_file)
    line_count = 0
    lines = []
    header = ''
    for row in csv_reader:
        if line_count==0:
            header = row
        elif row['set_subset']=='small':
            lines.append(row)
        line_count+=1
    input_file.close()

    random.shuffle(lines) #shuffle so each set has a good variety of data
    length = len(lines)
    testing_validation_length = int(length*0.2)
    training_length = length - 2*testing_validation_length

    header_row = ['track_id','track_genre_top','track_genres_all']

    training_file = open(output_file_training_name,'wb')
    training_writer = csv.writer(training_file,delimiter=',')
    training_writer.writerow(header_row)
    for line in lines[0:training_length]:
        training_writer.writerow([line['track_id'],line['track_genre_top'],line['track_genres_all']])
    training_file.close()

    testing_file = open(output_file_testing_name,'wb')
    testing_writer = csv.writer(testing_file,delimiter=',')
    testing_writer.writerow(header_row)
    for line in lines[training_length:training_length+testing_validation_length]:
        testing_writer.writerow([line['track_id'],line['track_genre_top'],line['track_genres_all']])
    testing_file.close()

    validation_file = open(output_file_validation_name,'wb')
    validation_writer = csv.writer(validation_file,delimiter=',')
    validation_writer.writerow(header_row)
    for line in lines[training_length+testing_validation_length:]:
        validation_writer.writerow([line['track_id'],line['track_genre_top'],line['track_genres_all']])
    validation_file.close()

def print_help():
    print('python divide_set.py data_file')
    print('      data_file      location of the tracks.csv file, or file of same format')

if __name__=='__main__':
    main()
