""" taken from https://github.com/HareeshBahuleyan/music-genre-classification/blob/master/generate_spectrograms.py

"""
import os

import matplotlib
#matplotlib.use('agg')

from matplotlib import pyplot as plt
from matplotlib import cm
from tqdm import tqdm
import pylab

import librosa
from librosa import display
import numpy as np

WAV_DIR = 'wav_files/'
IMG_DIR = 'spectrogram_images_small/'

#for f in tqdm(wav_files):
def get_spectro(f):
    try:
        # Read mp3-file
        y, sr = librosa.load(f, sr = 22050) # Use the default sampling rate of 22,050 Hz
        # Pre-emphasis filter
        pre_emphasis = 0.97
        y = np.append(y[0], y[1:] - pre_emphasis * y[:-1])
        
        # Compute spectrogram
        M = librosa.feature.melspectrogram(y, sr, 
                                           fmax = sr/2, # Maximum frequency to be used on the on the MEL scale
                                           n_fft=2048, 
                                           hop_length=512, 
                                           n_mels = 96, # As per the Google Large-scale audio CNN paper
                                           power = 2) # Power = 2 refers to squared amplitude
        
        # Power in DB
        log_power = librosa.power_to_db(M, ref=np.max)# Covert to dB (log) scale
        
        # Plotting the spectrogram and save as JPG without axes (just the image)
        pylab.figure(figsize=(3,3))
        pylab.axis('off') 
        pylab.axes([0., 0., 1., 1.], frameon=False, xticks=[], yticks=[]) # Remove the white edge
        librosa.display.specshow(log_power, cmap=cm.jet)
        pylab.savefig(IMG_DIR + f[-10:-4], bbox_inches=None, pad_inches=0)
        pylab.close()
    except Exception as e:
        print(f, e)
        pass

for root, dirs, files in os.walk("/Users/taylormurray/Desktop/music_genre_recognizer/datatest/fma_small"):
    if len(dirs) == 0:
        for f_ in files:
            filename = os.path.join(root, f_)
            get_spectro(filename)
