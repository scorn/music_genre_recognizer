import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.decomposition import PCA
import pickle

#Import features file
file = 'track_features.csv'

feature_df = pd.read_csv(file)

feature_df = feature_df.drop('Unnamed: 0', axis=1)

#Only keep top 8 genres
g = feature_df.groupby('genre').size().reset_index(name='counts').sort_values(by=[u'counts'],axis=0).tail(8)

for col in feature_df['genre'].unique():
	if col not in g['genre'].tolist():
		feature_df = feature_df.drop(feature_df.index[feature_df['genre']==col].tolist())

X = feature_df.drop('genre', axis=1)
X = X.astype(float)
#X = normalize(X)
Y = feature_df['genre']

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=23)


#create pipeline
pipe = Pipeline(steps=[('RFC', RandomForestClassifier(n_estimators=150))])
pipe.fit(X_train, Y_train)

predictions = pipe.predict(X_test)
print(accuracy_score(Y_test, predictions))

#export model
pickle.dump(pipe, open("rfc.p", "wb"), protocol=2)






