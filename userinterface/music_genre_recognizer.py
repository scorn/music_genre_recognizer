import sys
import feature
import numpy as np
import pandas as pd
import sklearn as sk
import pickle

# Task 1: Pass in Music File
# Task 2: Run Feature Extraction - make feature extractor
# Task 3: Format data? & Pass to model 
# ...

if __name__ == "__main__":
    
    # Take music file path from argument
    filename = sys.argv[1]

    # Pass file name to feature extractor: feature.py
    features = feature.extractFeatures(filename)

    # Flatten array


    # Turn features into a pandas dataframe
    df = pd.DataFrame(features)
    df = df.transpose()
    df.columns = df.columns.map(''.join)
    i = 0
    j = 0
    column_names = ['chroma_cens', 'chroma_cqt', 'chroma_stft', 'mfcc', 'rmse', 'spectral_bandwidth', 'spectral_centroid', 'spectral_contrast', 'spectral_rolloff', 'tonnetz', 'zcr']
    current = 'chroma_cens'
    for col in df.columns:
    	if current != col[0:len(current)]:
    		i = 0
    		j += 1
    		current = column_names[j]

    	if i > 0:
    		name = current + '.' + str(i)
    	elif i == 0:
    		name = current
    	df.rename(columns = {col:name}, inplace = True)
    	i += 1
    model = pickle.load( open("rfc.p", "rb") )
    predictions = model.predict(df)
    print(predictions)